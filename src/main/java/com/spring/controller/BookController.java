package com.spring.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.spring.model.Book;
import com.spring.service.BookService;

@RestController
public class BookController {
	
	
	@Autowired
	private BookService bookService;
	
	
	//Add new book
	@PostMapping("/book")
	public String save (@RequestBody Book b)
	{
		long id=bookService.save(b);
		return "Record saved with id: "+id;
	}
	
	
	/*//Get a book
	@GetMapping("/book/{id}")
	public ResponseEntity<?> get(@PathVariable("id") long id)
	{
		return 
	}*/

}
