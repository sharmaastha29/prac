package com.spring.dao;

import com.spring.model.Book;

public interface BookDao {

	public long save(Book b);
}
