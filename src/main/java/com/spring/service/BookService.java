package com.spring.service;

import com.spring.model.Book;

public interface BookService {

	public long save(Book b);
}
