package com.spring.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spring.dao.BookDao;
import com.spring.model.Book;

@Service	
public class BookServiceImpl implements BookService {
	@Autowired
	private BookDao bookdao;
	
	public long save(Book b){
	return bookdao.save(b);
	}

}
